from django.contrib import admin


from .models import Bin, Location


@admin.register(Bin)
@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    pass


# Register your models here.
