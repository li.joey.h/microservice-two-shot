# Wardrobify

Team:

* Joey Li - Shoes
* Rafael Guadron - Hats

## Design
Wardrobify is a RESTful API based projects that has two microservices, one for regarding shoes and one regarding hats. It uses PostgreSQL database to hold that data of all of the microservices, pollers to poll data from the Wardrobe API for bin and locations for the microservices, and React for the frontend application.

## Shoes microservice

Explain your models and integration with the wardrobe
microservice, here.

The shoes microservice has two models, the Shoes model that stores data on the shoes manufacturer,brand, color, a photo link, and the bin location. It also has the BinVo model that contains a href and closet name attribute that referneces the Bin model inside wardrobe monolith.

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.
