from django.urls import path
from .views import api_show_hat, api_list_hats


urlpatterns = [
    path("hats/<int:pk>/", api_show_hat, name="api_show_hat"),
    path("hats/", api_list_hats, name="api_list_hats"),
]
