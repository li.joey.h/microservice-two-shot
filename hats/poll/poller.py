import django
import os
import sys
import time
import json
import requests
from hats_rest.models import LocationVO

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()



def get_locations():
    response = requests.get("http://wardrobe-api:8000/api/locations/")
    content = json.loads(response.content)
    print("it works")
    for location in content["locations"]:
        LocationVO.objects.update_or_create(
            import_href=location["href"],
            defaults={"self_number": location["shelf_number"],
                      "section_number": location["section_number"],
                      "closet_name": location["closet_name"], }
        )


# Import models from hats_rest, here.
# from hats_rest.models import Something

def poll():
    while True:
        print('Hats poller polling for data')
        try:
            get_locations()
            pass
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
