import React, { useState, useEffect } from 'react';

function ShoesForm(props) {
  const [manufacturer, setManufacturer] = useState('');
  const [brand, setBrand] = useState('');
  const [color, setColor] = useState('');
  const [picture, setPicture] = useState('');
  const [bin, setBin] = useState('');
  const [bins, setBins] = useState([]);

  async function getBins() {
      const url = 'http://localhost:8100/api/bins/';

      const response = await fetch(url);
      if (response.ok) {
          const data = await response.json();
          setBins(data.bins);
      }
  }
  

  useEffect(() => {
      getBins();
  }, []);

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      manufacturer,
      brand,
      color,
      picture,
      bin
    };
  const locationUrl = 'http://localhost:8080/api/shoes/';
  const fetchConfig = {
    method: "post",
    body: JSON.stringify(data),
    headers: {
      'Content-Type': 'application/json',
    },
  };
  const response = await fetch(locationUrl, fetchConfig);
  if (response.ok) {
    const newShoe = await response.json();
    console.log(newShoe);
    setManufacturer('');
    setBrand('');
    setColor('');
    setPicture('');
    setBin('');
  }
  }

  function handleManufacturerChange(event) {
    const { value } = event.target;
    setManufacturer(value);
  }
  function handleBrandChange(event) {
    const { value } = event.target;
    setBrand(value);
  }
  function handleColorChange(event) {
    const { value } = event.target;
    setColor(value);
  }
  function handlePictureChange(event) {
    const { value } = event.target;
    setPicture(value);
  }
  function handleBin(event) {
    const { value } = event.target;
    setBin(value);
  }

return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add Shoes</h1>
          <form onSubmit={handleSubmit} id="create-shoe-form">
          <div className="form-floating mb-3">
              <input value={manufacturer} onChange={handleManufacturerChange} placeholder="manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
              <label htmlFor="manufacturer">manufacturer</label>
          </div>
          <div className="form-floating mb-3">
              <input value={brand} onChange={handleBrandChange} placeholder="brand" required type="text" name="brand " id="brand" className="form-control" />
              <label htmlFor="brand">brand </label>
          </div>
          <div className="form-floating mb-3">
              <input value={color} onChange={handleColorChange} placeholder="color" required type="text" name="color" id="color" className="form-control" />
              <label htmlFor="color">color</label>
          </div>
          <div className="form-floating mb-3">
              <input value={picture} onChange={handlePictureChange} placeholder="picture" required type="text" name="picture" id="picture" className="form-control" />
              <label htmlFor="picture">picture</label>
          </div>
          <div className="mb-3">
              <select value={bin} onChange={handleBin} required name="bin" id="bin" className="form-select">
                <option value="">Choose a bin</option>
                {bins.map((bin) => {
                  return (
                    <option key={bin.href} value={bin.href}>{bin.bin_number}</option>
                  )
                })}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoesForm;
