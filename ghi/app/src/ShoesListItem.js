import React from 'react';

function ShoesListItem({shoe}){
    async function handleDelete(id) {
        const shoeUrl = `http://localhost:8080/api/shoes/${id}`;
        const fetchConfig = {
          method: "delete",
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
          const newShoe = await response.json();
          console.log(newShoe);
        }
        }
    return(
        <div key={shoe.id} className=" card mb-3">
            <img src={shoe.picture} alt="" className="card-img-top"/>
            <div className="card-body shadow p-3 mb-5 bg-body-tertiary rounded">
                <h5 className="card-title">{shoe.brand}</h5>
                <h6 className="card-subtitle mb-2 text-muted">{shoe.manufacturer}</h6>
                <p className="card-text ">{shoe.bin.name}</p>
                <button key={`del${shoe.id}`} onClick={() =>handleDelete(shoe.id)} className="btn btn-danger">Delete</button>
            </div>
        </div>
    )
}

export default ShoesListItem;
