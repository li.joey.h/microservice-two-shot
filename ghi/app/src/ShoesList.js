import React, {useState, useEffect} from 'react';
import ShoesListItem from './ShoesListItem';
// use state and use effect has to be used in a component

function ShoesList(props) {

  const [shoes, setShoes] = useState([])

  async function fetchShoes() {
      const url = 'http://localhost:8080/api/shoes/';

      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setShoes(data.shoes)
      }

    }

  useEffect(() => {
      fetchShoes();
  }, [])
   const columns =[[],[],[]]
   shoes.forEach((shoe, idx) => {
    const colIdx = idx % 3;
    columns[colIdx].push(<ShoesListItem key={idx + shoe.manufacturer} shoe={shoe}/>)
   })
    return (
    <div className="row">
     <div className="container col-4">
      {columns[0]}
     </div>
     <div className="container col-4">
      {columns[1]}
     </div>
     <div className="container col-4">
      {columns[2]}
     </div>
    </div>


    );
}

  export default ShoesList;
