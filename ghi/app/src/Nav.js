import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-info">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">Wardrobify</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" id="shoeDropdownMain" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" to="/shoes">Shoes</NavLink>
              <div className="shoe-menu" aria-labelledby="shoeDropdownMain">
                <NavLink className="shoe_dropdown" to="/shoes/all">Shoe List</NavLink>
                <NavLink className="shoe_dropdown" to="/shoes/new">Add a Shoe</NavLink>
              </div>
            </li>
            <li className="nav-item-dropdown">
            <NavLink className="nav-link dropdown-toggle" id="hatDropdownMain" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" to="/hats">Hats</NavLink>
              <div className="hat-menu" aria-labelledby="hatDropdownMain">
                <NavLink className="hat_dropdown" to="/hats/all">Hat List</NavLink>
                <NavLink className="hat_dropdown" to="/hats/new">Add a Hat</NavLink>
              </div>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/shoes/new">Create New Shoes</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link" to="/hats">Hats</NavLink>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;

